#include "Assembler.h"

using namespace std;

    Assembler::Assembler(char* fileName ,char *outputFile){
        char *cstr = new char[strlen(fileName) + 1];
        strcpy(cstr, fileName);
        file.open(strcat(cstr,".mcr"));
        this->fileName = fileName;
        this->lineNumber = 0;
        this->currentTokenAddress = 0;
        this->sectionData = false;
        this->sectionText = false;
        this->currentSection = None;
    }

    Assembler::Assembler(string fileName, string outputFile){
        char *cstr = new char[fileName.length() + 1];
        strcpy(cstr, fileName.c_str());
        file.open(strcat(cstr, ".mcr"));
        this->fileName = fileName;
        this->outputFile = outputFile;
        this->lineNumber = 0;
        this->currentTokenAddress = 0;
        this->sectionData = false;
        this->sectionText = false;
        this->currentSection = None;
    }

    void Assembler::Start(){
        Analysis();

        try{
            TranslatetoIA32();
        }catch(int e){
            if(e == 20){
                cout << "Error:"+ to_string(e) + " - Missing command or empty line" << endl;
            }
        }
    }

    void Assembler::InitializeDerivativeTree(){

        DTHead dtHeadCommand;
        DT dtTerm, dtLabel, dtNumber, dtCommand;

        dtHead.command = "head";

        dtTerm.command = "term";
        dtTerm.type = Term;
        dtTerm.mandatory = true;

        dtNumber.command = "number";
        dtNumber.type = Number;
        dtNumber.mandatory = true;

        dtCommand.command = "variavle";
        dtCommand.type = Variable;
        dtCommand.mandatory = true;
        dtTerm.dev_tree.push_back(dtCommand);
        dtTerm.dev_tree.push_back(dtNumber);

        dtLabel.command = "label";
        dtLabel.type = Label;
        dtLabel.mandatory = false;

        dtCommand.command = "stop";
        dtCommand.type = Command;
        dtCommand.mandatory = true;

        dtHeadCommand.command = "stop";
        dtHeadCommand.type = Command;
        dtHeadCommand.dt.push_back(dtLabel);
        dtHeadCommand.dt.push_back(dtCommand);
        dtHead.dtCommand.push_back(dtHeadCommand);

        dtCommand.command = "copy";
        dtCommand.type = Command;
        dtCommand.mandatory = true;

        dtHeadCommand.command = "copy";
        dtHeadCommand.type = Command;
        dtHeadCommand.dt.clear();
        dtHeadCommand.dt.push_back(dtLabel);
        dtHeadCommand.dt.push_back(dtCommand);
        dtHeadCommand.dt.push_back(dtTerm);
        dtHeadCommand.dt.push_back(dtTerm);
        dtHead.dtCommand.push_back(dtHeadCommand);

        for(unsigned int i = 0; i < commands.size(); i++){
            if(commands[i].compare("copy") && commands[i].compare("stop") && commands[i].compare("space") && commands[i].compare("const") && commands[i].compare("section")){
                dtCommand.command = commands[i];
                dtCommand.type = Command;
                dtCommand.mandatory = true;

                dtHeadCommand.command = commands[i];
                dtHeadCommand.type = Command;
                dtHeadCommand.dt.clear();
                dtHeadCommand.dt.push_back(dtLabel);
                dtHeadCommand.dt.push_back(dtCommand);
                dtHeadCommand.dt.push_back(dtTerm);
                dtHead.dtCommand.push_back(dtHeadCommand);
            }
        }

        dtCommand.command = "space";
        dtCommand.type = Command;
        dtCommand.mandatory = true;

        dtLabel.mandatory = true;
        dtNumber.mandatory = false;

        dtHeadCommand.command = "space";
        dtHeadCommand.type = Command;
        dtHeadCommand.dt.clear();
        dtHeadCommand.dt.push_back(dtLabel);
        dtHeadCommand.dt.push_back(dtCommand);
        dtHeadCommand.dt.push_back(dtNumber);
        dtHead.dtCommand.push_back(dtHeadCommand);

        dtCommand.command = "const";
        dtCommand.type = Command;
        dtCommand.mandatory = true;

        dtLabel.mandatory = true;
        dtNumber.mandatory = true;

        dtHeadCommand.command = "const";
        dtHeadCommand.type = Command;
        dtHeadCommand.dt.clear();
        dtHeadCommand.dt.push_back(dtLabel);
        dtHeadCommand.dt.push_back(dtCommand);
        dtHeadCommand.dt.push_back(dtNumber);
        dtHead.dtCommand.push_back(dtHeadCommand);

        dtCommand.command = "section";
        dtCommand.type = Command;
        dtCommand.mandatory = true;

        dtHeadCommand.command = "section";
        dtHeadCommand.type = Command;
        dtHeadCommand.dt.clear();
        dtHeadCommand.dt.push_back(dtCommand);
        dtCommand.command = "sectionElement";
        dtCommand.type = SectionElement;
        dtCommand.mandatory = true;
        dtHeadCommand.dt.push_back(dtCommand);
        dtHead.dtCommand.push_back(dtHeadCommand);
    }

    void Assembler::InitializeIA32Map(){

        this->maptoIA32.insert(make_pair("section", "section "));
        this->maptoIA32.insert(make_pair("text", ".text"));
        this->maptoIA32.insert(make_pair("global", "global _start"));
        this->maptoIA32.insert(make_pair("start", "_start:"));
        this->maptoIA32.insert(make_pair("bss", ".bss"));
        this->maptoIA32.insert(make_pair("data", ".data"));

        this->maptoIA32.insert(make_pair("space", "%s resd %s"));
        this->maptoIA32.insert(make_pair("const", "%s dd %s"));

        /** Como no assembly inventado soma o que foi fornecido com o acumulador ao traduzir para
            o IA32 o primeiro parâmetro do add deve ser o registrador EAX*/
        /* OBS: Passar o primeiro parâmentro do add do Assembly inventado para o EAX*/
        //add dest source
        this->maptoIA32.insert(make_pair("add", "add %s,%s"));

        /** Como no assembly inventado subtrai o que foi fornecido com o acumulador ao traduzir para
            o IA32 o primeiro parâmetro do sub deve ser o registrador EAX*/
        /* OBS: Passar o primeiro parâmetro sub do Assembly inventado para o EAX */
        //sub dest source
        this->maptoIA32.insert(make_pair("sub", "sub %s,%s"));

        /** A multiplicação em IA32 utiliza o que está no registrador eax com o que foi passado no mul*/
        /* OBS: Passar o primeiro parâmetro do mult do Assembly inventado para o EAX*/
        //mul reg
        this->maptoIA32.insert(make_pair("mult","mul %s"));

        /** A divisão em IA32 utiliza o registrador AX (16 bits do registrador EAX) como dividendo e o registrador
            que foi passado no div como divisor*/
        //div reg
        this->maptoIA32.insert(make_pair("div", "div %s"));

        /** Jump incondicional*/
        // jmp label
        this->maptoIA32.insert(make_pair("jmp", "jmp %s"));

        /** O JMPN (jump if negative) do assembly inventado verifica o acumulador enquanto no IA32 necessita a chamada ao comando
        CMP para acionar flaps para a comparação no comando JB (jump if below)*/
        // jb label
        this->maptoIA32.insert(make_pair("jmpn", "jl %s"));

        this->maptoIA32.insert(make_pair("jmpp", "jg %s"));

        this->maptoIA32.insert(make_pair("jmpz", "jz %s"));

        this->maptoIA32.insert(make_pair("jae", "jae %s"));

        this->maptoIA32.insert(make_pair("load", "mov eax,%s"));

        this->maptoIA32.insert(make_pair("store", "mov %s,eax"));

        this->maptoIA32.insert(make_pair("move", "mov %s,%s"));

        this->maptoIA32.insert(make_pair("movedd", "mov dword %s, %s"));

        this->maptoIA32.insert(make_pair("systemcall", "int 80h"));

        this->maptoIA32.insert(make_pair("input", "mov eax,3"));

        this->maptoIA32.insert(make_pair("copy", "mov eax, %s \nmov dword %s, eax"));

        this->maptoIA32.insert(make_pair("output", "mov eax,4"));

        this->maptoIA32.insert(make_pair("c_input", "mov eax,3"));

        this->maptoIA32.insert(make_pair("c_output", "mov eax,4"));

        this->maptoIA32.insert(make_pair("s_input", "mov eax,3"));

        this->maptoIA32.insert(make_pair("s_output", "mov eax,4"));

        this->maptoIA32.insert(make_pair("stop", "mov eax,1"));

        ///Recebe um endereço de memória
        this->maptoIA32.insert(make_pair("push", "push %s"));

        ///Recebe um endereço de memória
        this->maptoIA32.insert(make_pair("pop", "pop %s"));

        this->maptoIA32.insert(make_pair("inc", "inc %s"));

        this->maptoIA32.insert(make_pair("dec", "dec %s"));

        this->maptoIA32.insert(make_pair("cmp", "cmp %s,%s"));
    }

    void Assembler::InitializaCommandCode(){

        CommandCode cc;

        for(unsigned int i = 0; i < dataCommands.size(); i++){
            cc.name = dataCommands[i];
            cc.code = i + 1;
            commandCode.push_back(cc);
        }


    }

    void Assembler::Analysis(){

        vector<string> *outVector = new vector<string>();
        vector<Token> tokens;
        string line;

        if(!file.is_open()){
            throw "Parser exception. Can not open file .mcr";
        }

        while(getline(file, line)){

            Utilities::SplitString(line, " ,", outVector);
            this->lineNumber++;


            //Lexicon analysis
            for(unsigned int i = 0; i < outVector->size(); i++){
                AnalysisToken(outVector->at(i), tokens);
            }

            AnalysisSemantics(tokens);

            AssembleTS(tokens);

            outVector->clear();
            tokens.clear();
        }

        if(!this->sectionData){
            ErrorModel("Missing section Data", Syntactic);
        }

        if(!this->sectionText){
            ErrorModel("Missing section Text", Syntactic);
        }

        file.clear(); // clear bad state after eof
        file.seekg(0);
        this->lineNumber = 0;
    }

    void Assembler::AnalysisToken(const string token, vector<Token> &out){
        Token newToken;

        newToken.name = token;
        newToken.validated = false;

        if(token.find(':')  != string::npos){
            newToken.type = Label;
        }
        else if(token.at(0) == '0'){
            if(token.at(1) == 'x'){
                string number = token.substr(2, string::npos);
                if(Utilities::is_number(number, 16)){//hexadecimal
                    newToken.type = Number;
                }else{
                    string message = "Line " + to_string(lineNumber) + ": Invalid token. Invalid caracter " + token.at(0)
                                        + ", token can not start with a number.";
                    ErrorModel(message, Lexicon);
                }
            }else{
                    string message = "Line " + to_string(lineNumber) + ": Invalid token. Invalid caracter " + token.at(0)
                                        + ", token can not start with a number.";
                    ErrorModel(message, Lexicon);
            }
        }else if(Utilities::is_number(token, 10)){//Decimal
            newToken.type = Number;
        }else if(!token.compare("data") || !token.compare("text")){
            newToken.type = SectionElement;
        }else{
            newToken.type = Variable;
        }


        for(unsigned int i = 0; i < commands.size(); i++){
            if(!token.compare(commands[i])){
                newToken.type = Command;
                break;
            }
        }

        for(unsigned int i = 0; i < invalidASC.size(); i++){
            if(token.find(invalidASC[i]) != string::npos){
                string message = "Line " + to_string(lineNumber) + ": Invalid token. Invalid caracter " + invalidASC[i] + " in " + token + ".";
                ErrorModel(message, Lexicon);
            }
        }

        if(token.find_last_of("123456789", 0) != string::npos && newToken.type != Number){
            string message = "Line " + to_string(lineNumber) + ": Invalid token. Invalid caracter " + token.at(0)
                                + ". token can not start with a number.";
            ErrorModel(message, Lexicon);
        }

        out.push_back(newToken);

    }

    void Assembler::AnalysisSemantics(vector<Token> &tokens){
        DerivationTreeHead devTree;

        if(dtHead.dtCommand.empty()){
            InitializeDerivativeTree();
        }


        ///Busca pelo command
        vector<Token>::iterator command = find_if(tokens.begin(), tokens.end(), Utilities::isCommand);

        for(unsigned int i = 0; i < dtHead.dtCommand.size(); i++){
            if(!command->name.compare(dtHead.dtCommand[i].command)){
                devTree = dtHead.dtCommand[i];
            }
        }

        ///Verificando ávore de derivação
        int indexToken = 0;
        bool valideCommand = true;
        if(command == tokens.end() && !tokens.empty()){
            string message = "Line " + to_string(lineNumber) + ": Syntax error. Missing command.";
            ErrorModel(message, Syntactic);
        }else{

            for(unsigned int i = 0; i < devTree.dt.size(); i++){
                if(devTree.dt[i].type == tokens[indexToken].type){
                    tokens[indexToken].validated = true;
                    indexToken++;
                }else if(!devTree.dt[i].dev_tree.empty() && indexToken < tokens.size()){
                    for(unsigned int j = 0; j < devTree.dt[i].dev_tree.size(); j++){
                        if(devTree.dt[i].dev_tree[j].type == tokens[indexToken].type){
                            tokens[indexToken].validated = true;
                            indexToken++;
                            break;
                        }
                    }

                }else if(devTree.dt[i].mandatory){
                    string message = "Line " + to_string(lineNumber) + ": Syntax error.Term " + devTree.dt[i].command + " is mandatory in command " +  devTree.command;
                    ErrorModel(message, Syntactic);
                    valideCommand = false;
                    break;
                }
            }

            for(unsigned int i = 0; i < tokens.size(); i++){

                if(!tokens[i].validated){
                    string message = "Line " + to_string(lineNumber) + ": Syntax error near command " + command->name + ".";
                    ErrorModel(message, Syntactic);
                }
            }

        }
        ///end

        ///Calculando endereços
        for(unsigned int i = 0; i < tokens.size(); i++){
            if(tokens[i].type != Label && tokens[i].name.compare("const") != 0 && devTree.command.compare("section") != 0 && tokens[i].type != SectionElement){
                if(tokens[i].name.compare("space") == 0 && tokens.size() == 3 && valideCommand){
                    tokens[i].address = this->currentTokenAddress;
                    this->currentTokenAddress += atoi(tokens[i + 1].name.c_str());
                    break;
                }else{
                    tokens[i].address = this->currentTokenAddress;
                    this->currentTokenAddress++;
                }
            }else{
                tokens[i].address = this->currentTokenAddress;
            }

            if(!devTree.command.compare("section") && tokens[i].type == SectionElement && valideCommand){
                if(!tokens[i].name.compare("data")){
                    this->currentSection = Data;
                    this->sectionData = true;
                    if(sectionText){
                        string message = "Section data must come before section text.";
                        //ErrorModel(message, Semantic);
                    }
                }else{
                    this->currentSection = Text;
                    this->sectionText = true;
                }
            }
        }
        ///end

        bool errorSection = true;
        string message;
        if(this->currentSection == Data && command != tokens.end()){
            for(unsigned int i = 0; i < dataCommands.size(); i++){
                message = "Line " + to_string(lineNumber) + ": Syntax error. Command " + command->name + " need to be inside section Data.";
                if(!command->name.compare(dataCommands[i]) || !command->name.compare("section") || command->type != Command){
                    errorSection = false;
                    break;
                }
            }
        }else if(this->currentSection == Text && command != tokens.end()){
            for(unsigned int i = 0; i < textCommands.size(); i++){
                message = "Line " + to_string(lineNumber) + ": Syntax error. Command " + command->name + " need to be inside section Text.";
                if(!(command->name.compare(textCommands[i])) || !(command->name.compare("section")) || command->type != Command){
                    errorSection = false;
                    break;
                }
            }
        }

        if(errorSection && command != tokens.end()){
            ErrorModel(message, Syntactic);
        }
    }

    void Assembler::AssembleTS(vector<Token> tokens){

        vector <Token>::iterator label = find_if(tokens.begin(), tokens.end(), Utilities::isLabel);

        if(label != tokens.end()){
            if(find_if(tSymbols.begin(), tSymbols.end(), [=](TSymbols t){ return (t.labelName == label->name);} ) == tSymbols.end()){
                TSymbols ts;
                label->name.erase(remove(label->name.begin(), label->name.end(), ':'), label->name.end());
                ts.labelName = label->name;
                ts.address = label->address;
                tSymbols.push_back(ts);
            }else{
                string message = "Line " + to_string(lineNumber) + ": Redeclaration of " + label->name;
                ErrorModel(message, Semantic);
            }
        }
    }

    void Assembler::AssemblerCode(){

        vector<string> *outVector = new vector<string>();
        vector<Token> tokens;
        string line, str;

        InitializaCommandCode();
        char *cstr = new char[this->outputFile.length() + 1];
        strcpy(cstr, outputFile.c_str());
        ofstream outFile (strcat(cstr, ".o"));

        this->lineNumber = 0;
        file.clear(); // clear bad state after eof
        file.seekg(0);

        while(getline(file, line)){
            str.clear();

            Utilities::SplitString(line, " ,", outVector);
            this->lineNumber++;

            for(unsigned int i = 0; i < outVector->size(); i++){
                AnalysisToken(outVector->at(i), tokens);
            }
            int loop = 0;
            for(unsigned int i = 0; i < tokens.size(); i++){
                if(tokens[i].type == Command && tokens[i].type != SectionElement && tokens[i].name.compare("section") != 0 ){
                    if(tokens.at(i).name.compare("space") == 0){
                        str += to_string(0);
                        str += to_string(0);
                        if(tokens.size() == 3){
                            loop = stoi(tokens[i + 1].name);
                        }else{
                            loop = 1;
                        }

                    }else if(tokens[i].name.compare("const") == 0){
                        str += tokens[i + 1].name;
                        loop = 1;
                    }else{
                        for(unsigned int j = 0; j < commandCode.size(); j++){
                            if(tokens[i].name.compare(commandCode[j].name) == 0){
                                str += to_string(commandCode[j].code);
                                loop = 1;
                                break;
                            }
                        }
                    }

                }else if(tokens[i].type == Variable){
                    bool valid = false;
                    for(unsigned int j = 0; j < tSymbols.size(); j++){
                        if(tokens[i].name.compare(tSymbols[j].labelName) == 0){
                            str += " " + to_string(tSymbols[j].address);
                            loop = 1;
                            valid = true;
                            break;
                        }
                    }

                    if(!valid){
                        string message = "Line " + to_string(lineNumber) + ": Semanti error. Label " + tokens[i].name + " was not declared.";
                        ErrorModel(message, Semantic);
                    }
                }

            }

            if(!str.empty()){
                for(int i = 0; i < loop; i++){
                    outFile << str << " ";
                }
            }

            outVector->clear();
            tokens.clear();
        }
    }

    void Assembler::TranslatetoIA32(){
        int outputN = 0, inputN = 0, c_outputN = 0, c_inputN = 0, s_outputN = 0, s_inputN = 0;
        vector<string> *outVector = new vector<string>();
        vector<Token> tokens;
        string line, str;
        vector<vector<Token>> data, bss, text;
        char buffer[100];

        InitializeIA32Map();
        char *cstr = new char[this->outputFile.length() + 1];
        strcpy(cstr, outputFile.c_str());
        ofstream outFile (strcat(cstr, ".s"));

        file.clear(); // clear bad state after eof
        file.seekg(0);

        while(getline(file, line)){

            Utilities::SplitString(line, " ,", outVector);

            for(unsigned int i = 0; i < outVector->size(); i++){
                AnalysisToken(outVector->at(i), tokens);
            }

            ///Busca pelo command
            vector<Token>::iterator command = find_if(tokens.begin(), tokens.end(), Utilities::isCommand);

            if(command == tokens.end() && !tokens.empty()){
                outVector->clear();
                continue;
            }else if(command == tokens.end() && tokens.empty()){
                outVector->clear();
                tokens.clear();
                continue;
            }

            if(!command->name.compare("space")){
                bss.push_back(tokens);
            }else if(!command->name.compare("const")){
                data.push_back(tokens);
            }else{
                text.push_back(tokens);
            }

            outVector->clear();
            tokens.clear();
        }

        ///.data
        outFile << this->maptoIA32.find("section")->second << this->maptoIA32.find("data")->second << endl;
        outFile << "msgerrorS_INPUT db 'Tentativa de acesso a memoria nao reservada', 0dH, 0ah" << endl;
        outFile << "msgerrorSIZE EQU $-msgerrorS_INPUT" << endl;

        for(unsigned int i = 0; i < data.size(); i++){
            str.clear();
            str = "";

            auto command = find_if(data[i].begin(), data[i].end(), Utilities::isCommand);
            auto mapIA32 = this->maptoIA32.find(command->name);
            if(mapIA32 == this->maptoIA32.end()){
                outFile.close();
                throw;
            }
            data[i][0].name.erase(remove(data[i][0].name.begin(), data[i][0].name.end(), ':'), data[i][0].name.end());
            int cx = snprintf(buffer, 100, mapIA32->second.c_str(), data[i][0].name.c_str(), data[i][2].name.c_str());
            str += buffer;
            if(data[i][2].name.find("'") != string::npos){
                str += ", 0dH, 0ah";
            }

            outFile << str << endl;

        }

        ///.bss
        outFile << this->maptoIA32.find("section")->second << this->maptoIA32.find("bss")->second << endl;
        outFile << "inputVariable resb 11" << endl;// variavel utilizada pelo input
        outFile << "s_inputVariable resd 100" << endl;
        outFile << "addr_aux resw 1" << endl;

        for(unsigned int i = 0; i < bss.size(); i++){
            auto command = find_if(bss[i].begin(), bss[i].end(), Utilities::isCommand);
            auto mapIA32 = this->maptoIA32.find(command->name);
            if(mapIA32 == this->maptoIA32.end()){
                throw;
            }
            bss[i][0].name.erase(remove(bss[i][0].name.begin(), bss[i][0].name.end(), ':'), bss[i][0].name.end());
            if(bss[i].size() > 2){
                int cx = snprintf(buffer, 100, mapIA32->second.c_str(), bss[i][0].name.c_str(), bss[i][2].name.c_str());
                outFile << buffer << endl;
            }else{
                int cx = snprintf(buffer, 100, mapIA32->second.c_str(), bss[i][0].name.c_str(), "1");
                outFile << buffer << endl;
            }
        }

        ///.text
        outFile << this->maptoIA32.find("section")->second << this->maptoIA32.find("text")->second << endl;
        outFile << this->maptoIA32.find("global")->second << endl;
        outFile << this->maptoIA32.find("start")->second << endl;

        for(unsigned int i = 0; i < text.size(); i++){
            str.clear();
            str = "";
            auto command = find_if(text[i].begin(), text[i].end(), Utilities::isCommand);
            auto label = find_if(text[i].begin(), text[i].end(), Utilities::isLabel);
            auto mapIA32 = this->maptoIA32.find(command->name);
            if(mapIA32 == this->maptoIA32.end()){
                throw;
            }

            int index = 1; // index do primeiro argumento sem label
            if(label != text[i].end()){
            	str += label->name;
                str += " ";
                index++;
            }

            if(!mapIA32->first.compare("add") || !mapIA32->first.compare("sub")){

                string address("[" + text[i][index].name + "]");
                int cx = snprintf(buffer, 100, mapIA32->second.c_str(), "eax", address.c_str());
                str += buffer;
                outFile << str << endl;

            }else if(!mapIA32->first.compare("mult") || !mapIA32->first.compare("div")){

                string address("[" + text[i][index].name + "]");
                int cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", address.c_str());
                str += buffer;
                outFile << str << endl;

                cx = snprintf(buffer, 100, mapIA32->second.c_str(), "ecx");
                outFile << buffer << endl;

            }else if(!mapIA32->first.compare("jmp")){

                int cx = snprintf(buffer, 100, mapIA32->second.c_str(), text[i][index].name.c_str());
                str += buffer;
                outFile << str << endl;
            }else if(!mapIA32->first.compare("jmpn") || !mapIA32->first.compare("jmpp") || !mapIA32->first.compare("jmpz")){

                int cx = snprintf(buffer, 100, mapIA32->second.c_str(), text[i][index].name.c_str());
                str += buffer;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", "0");
                outFile << buffer << endl;

                outFile << "cmp eax,0" << endl;
                outFile << str << endl;
            }else if(!mapIA32->first.compare("copy")){

                string dest("[" + text[i][index].name + "]");
                string source("[" + text[i][index+1].name + "]");
                int cx = snprintf(buffer, 100, mapIA32->second.c_str(), source.c_str(), dest.c_str());
                str += buffer;

                outFile << str << endl;
            }else if(!mapIA32->first.compare("load") || !mapIA32->first.compare("store")){

                string address("[" + text[i][index].name + "]");
                int cx = snprintf(buffer, 100, mapIA32->second.c_str(), address.c_str());
                str += buffer;

                outFile << str << endl;
            }else if(!mapIA32->first.compare("input")){
                string strAUX;
                inputN++;
                outFile << ";Inicio input" << endl;

                ///Guandando estado registradores
                int cx = snprintf(buffer, 100, this->maptoIA32.find("push")->second.c_str(), "eax");
                str += buffer;
                outFile << str << endl;

                ///Iniciando chamada ao sistema
                outFile << mapIA32->second << endl;

                //primeiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "0");
                outFile << buffer << endl;

                //segundo argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", "inputVariable");
                outFile << buffer << endl;

                //terceiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "11");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;

                ///Convertendo ascii para decimal
                outFile << "xor esi,esi" << endl;//Zerando registrador esi
                outFile << "xor ecx,ecx" << endl;//zerando

                outFile << "loop" << to_string(inputN) << "ASCII2DecimalLABEL:" << endl; //Label loop 1

                outFile << "xor eax,eax" << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "inputVariable");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "al", "[inputVariable + esi]");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("cmp")->second.c_str(), "eax", "10");
                outFile << buffer << endl;

                strAUX = "inputLABEL" + to_string(inputN);
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmpz")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("sub")->second.c_str(), "eax", "48");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("push")->second.c_str(), "eax");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("inc")->second.c_str(), "esi");
                outFile << buffer << endl;

                strAUX = "loop" + to_string(inputN) + "ASCII2DecimalLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmp")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                outFile << "inputLABEL" << to_string(inputN) << ":" << endl;

                //Passando número lido para variável
                string address("[" + text[i][index].name + "]");
                cx = snprintf(buffer, 100, this->maptoIA32.find("movedd")->second.c_str(), address.c_str(), "0");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", "1");
                outFile << buffer << endl;

                outFile << "lloop" << to_string(inputN) << "ASCII2DecimalLABEL:" << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("cmp")->second.c_str(), "esi", "0");
                outFile << buffer << endl;

                strAUX = "end" + to_string(inputN) + "ASCII2DecimalLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmpz")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("pop")->second.c_str(), "eax");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("mult")->second.c_str(), "ecx");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("add")->second.c_str(), address.c_str(), "eax");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "eax", "10");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("mult")->second.c_str(), "ecx");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", "eax");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("dec")->second.c_str(), "esi");
                outFile << buffer << endl;

                strAUX = "lloop" + to_string(inputN) + "ASCII2DecimalLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmp")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                outFile << "end" << to_string(inputN) << "ASCII2DecimalLABEL:" << endl;
                ///Retornando registradores para seus números originais
                cx = snprintf(buffer, 100, this->maptoIA32.find("pop")->second.c_str(), "eax");
                outFile << buffer << endl;

                outFile << ";Fim input" << endl;

            }else if(!mapIA32->first.compare("output")){
                string strAUX;
                outputN++;
                outFile << ";Início Output" << endl;
                ///Guandando estado registradores
                int cx = snprintf(buffer, 100, this->maptoIA32.find("push")->second.c_str(), "eax");
                str += buffer;
                outFile << str << endl;

                ///Convertendo decimal para ascii, armazenado cada número na stack
                outFile << "xor esi,esi" << endl;//Zerando registrador esi

                string address("[" + text[i][index].name + "]");

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "eax", address.c_str());
                outFile << buffer << endl;

                outFile << "loop" << to_string(outputN)<< "Decimal2ASCIILABEL:" << endl; //Label loop 1

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "0");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "10");//Irá dividir por 10
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("div")->second.c_str(), "ebx");//Divisão por 10, irá pegar o resto
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("add")->second.c_str(), "edx", "48");//Convertendo para ascii
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("push")->second.c_str(), "edx");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("inc")->second.c_str(), "esi");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("cmp")->second.c_str(), "eax", "0");
                outFile << buffer << endl;

                strAUX = "lloop" + to_string(outputN) + "Decimal2ASCIILABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmpz")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                strAUX = "loop" + to_string(outputN) + "Decimal2ASCIILABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmp")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                ///Printando número
                outFile << "lloop" << to_string(outputN) << "Decimal2ASCIILABEL:" << endl; //Label loop 2


                cx = snprintf(buffer, 100, this->maptoIA32.find("cmp")->second.c_str(), "esi", "0");
                outFile << buffer << endl;

                strAUX = "end" + to_string(outputN) + "Decimal2ASCIILABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmpz")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("dec")->second.c_str(), "esi");
                outFile << buffer << endl;

                //iniciando chamada ao sistema
                outFile << mapIA32->second << endl; // passando para eax códido da sys_call

                //primeiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "1");
                outFile << buffer << endl;

                //segundo argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", "esp"); //recebe topo da stack, número mais significativo
                outFile << buffer << endl;

                //terceiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "1");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("add")->second.c_str(), "esp", "4");//movendo stack para próximo número
                outFile << buffer << endl;

                strAUX = "lloop" + to_string(outputN) + "Decimal2ASCIILABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmp")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                ///END: Retornando registradores para estado natural
                outFile << "end" << to_string(outputN) << "Decimal2ASCIILABEL:" << endl;

                ///Retornando registradores para seus números originais
                cx = snprintf(buffer, 100, this->maptoIA32.find("pop")->second.c_str(), "eax");
                outFile << buffer << endl;

                outFile << ";Fim Output" << endl;

            }else if(!mapIA32->first.compare("c_input")){

                outFile << "; Início c_input" << endl;
                ///Guandando estado registradores
                int cx = snprintf(buffer, 100, this->maptoIA32.find("push")->second.c_str(), "eax");
                str += buffer;
                outFile << str << endl;

                ///chamada ao sistema
                outFile << mapIA32->second << endl;

                //primeiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "0");
                outFile << buffer << endl;

                //segundo argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", text[i][index].name.c_str()); //recebe topo da stack, número mais significativo
                outFile << buffer << endl;

                //terceiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "1");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;

                ///Retornando registradores para seus números originais
                cx = snprintf(buffer, 100, this->maptoIA32.find("pop")->second.c_str(), "eax");
                outFile << buffer << endl;

                outFile << ";Fim c_input" << endl;

            }else if(!mapIA32->first.compare("c_output")){

                outFile << "; Início c_output" << endl;

                ///Guandando estado registradores
                int cx = snprintf(buffer, 100, this->maptoIA32.find("push")->second.c_str(), "eax");
                str += buffer;
                outFile << str << endl;

                ///Início chamada de sistema
                outFile << mapIA32->second << endl;

                //primeiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "1");
                outFile << buffer << endl;

                //segundo argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", text[i][index].name.c_str());
                outFile << buffer << endl;

                //terceiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "1");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;

                ///Retornando registradores para seus números originais
                cx = snprintf(buffer, 100, this->maptoIA32.find("pop")->second.c_str(), "eax");
                outFile << buffer << endl;

                outFile << "; Fim c_output" << endl;

            }else if(!mapIA32->first.compare("s_input")){
                string strAUX;
                s_inputN++;
                outFile << ";Início s_input" << endl;

                ///Guandando estado registradores
                int cx = snprintf(buffer, 100, this->maptoIA32.find("push")->second.c_str(), "eax");
                str += buffer;
                outFile << str << endl;

                ///Chamada ao sistema
                outFile << mapIA32->second << endl;

                //primeiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "0");
                outFile << buffer << endl;

                //segundo argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", "s_inputVariable");
                outFile << buffer << endl;

                //terceiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "100");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;


                ///Contando quantidade de digitos
                outFile << "xor esi,esi" << endl;//Zerando registrador esi

                outFile << "loop"<< to_string(s_inputN) << "S_INPUTLABEL:" << endl; //Label loop 1

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "eax", "[s_inputVariable + esi]");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("cmp")->second.c_str(), "eax", "10");
                outFile << buffer << endl;

                strAUX = "S_INPUTLABEL" + to_string(s_inputN);
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmpz")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("inc")->second.c_str(), "esi");
                outFile << buffer << endl;

                strAUX = "loop" + to_string(s_inputN) + "S_INPUTLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmp")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                outFile << "S_INPUTLABEL" << to_string(s_inputN) << ":" << endl;
                string tam = "1";
                for(unsigned int j = 0; j < data.size(); j++){
                    if(!bss[j][0].name.compare(text[i][index].name) && bss[j].size() > 2){
                        tam = bss[j][2].name;
                        break;
                    }
                }

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", tam.c_str());
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("cmp")->second.c_str(), "ebx", "esi");
                outFile << buffer << endl;

                strAUX = "end" + to_string(s_inputN) + "S_INPUTLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jae")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                ///Error
                //Print do erro
                outFile << this->maptoIA32.find("s_output")->second << endl;

                //primeiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "1");
                outFile << buffer << endl;

                //segundo argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", "msgerrorS_INPUT");
                outFile << buffer << endl;

                //terceiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "msgerrorSIZE");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;

                //Stop
                outFile << this->maptoIA32.find("stop")->second << endl;

                //primeiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "0");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;


                outFile <<  strAUX << ":" << endl;

                outFile << "xor ebx,ebx" << endl;//Zerando registrador ebx
                string aux = "s_inputVariable";
                string address1("[" + text[i][index].name + "+" + "ebx" + "*4]");
                string address2("[" + aux + "+" + "ebx" + "*4]");

                cx = snprintf(buffer, 100, this->maptoIA32.find("movedd")->second.c_str(), address1.c_str(), "0");
                outFile << buffer << endl;

                outFile << "lloop" << to_string(s_inputN) << "S_INPUTLABEL:" << endl; //Label loop 2

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", address2.c_str());
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), address1.c_str(), "ecx");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("inc")->second.c_str(), "ebx");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("cmp")->second.c_str(), "ebx", "esi");
                outFile << buffer << endl;

                strAUX = "END" + to_string(s_inputN) + "S_INPUTLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmpp")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                strAUX = "lloop" + to_string(s_inputN) + "S_INPUTLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmp")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                outFile << "END"<< to_string(s_inputN) << "S_INPUTLABEL:" << endl; //Label loop 1

                cx = snprintf(buffer, 100, this->maptoIA32.find("movedd")->second.c_str(), address1.c_str(), "13");
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("pop")->second.c_str(), "eax");
                outFile << buffer << endl;

                outFile << ";Fim s_input" << endl;
            }else if(!mapIA32->first.compare("s_output")){
                string strAUX;
                s_outputN++;
                outFile << ";Início s_output" << endl;

                ///Guandando estado registradores
                int cx = snprintf(buffer, 100, this->maptoIA32.find("push")->second.c_str(), "eax");
                str += buffer;
                outFile << str << endl;

                ///Iniciando chamada ao sistema
                outFile << "xor esi,esi" << endl;//Zerando registrador esi
                string address1("[" + text[i][index].name + "+" + "esi" + "]");
                string address2(text[i][index].name + "+" + "esi" + "*4");

                outFile << "loop" << to_string(s_outputN) << "S_OUTPUTLABEL:" << endl; //Label loop 1

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "eax", address1.c_str());
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("cmp")->second.c_str(), "eax", "13");
                outFile << buffer << endl;

                strAUX = "end" + to_string(s_outputN) + "S_OUTPUTLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmpz")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "[addr_aux]", "eax");
                outFile << buffer << endl;

                outFile << mapIA32->second << endl;

                //primeiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "0");
                outFile << buffer << endl;

                //segundo argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ecx", "addr_aux");
                outFile << buffer << endl;

                //terceiro argumento
                cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "edx", "1");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("inc")->second.c_str(), "esi");
                outFile << buffer << endl;

                strAUX = "loop" + to_string(s_outputN) + "S_OUTPUTLABEL";
                cx = snprintf(buffer, 100, this->maptoIA32.find("jmp")->second.c_str(), strAUX.c_str());
                outFile << buffer << endl;

                outFile << "end" << to_string(s_outputN) <<"S_OUTPUTLABEL:" << endl;

                cx = snprintf(buffer, 100, this->maptoIA32.find("pop")->second.c_str(), "eax");
                outFile << buffer << endl;

                outFile << ";Fim s_output" << endl;

            }else if(!mapIA32->first.compare("stop")){

                str += mapIA32->second;
                outFile << str << endl;

                //primeiro argumento
                int cx = snprintf(buffer, 100, this->maptoIA32.find("move")->second.c_str(), "ebx", "0");
                outFile << buffer << endl;

                outFile << this->maptoIA32.find("systemcall")->second.c_str() << endl;
            }
        }

        cout << "FIM" << endl;
        outFile.close();
    }

    void Assembler::ErrorModel (string message, ErrorType type) {
        Error newError;
        newError.line = lineNumber;
        newError.type = type;
        newError.message = message;
        error.push_back(newError);
    }
