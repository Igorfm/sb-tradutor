#ifndef STRUCTS_H
#define STRUCTS_H

#include<string>
#include "Enums.h"

typedef struct Token {
    std::string name;
    TokenType type;
    int address;
    bool validated;
}Token;

typedef struct Error {
    int type;
    int line;
    std::string message;
}Error;

typedef struct DerivationTree{
    std::string command;
    TokenType type;
    bool mandatory;
    std::vector<struct DerivationTree> dev_tree;
}DT;

typedef struct DerivationTreeHead {
    std::string command;
    TokenType type;
    std::vector<struct DerivationTreeHead> dtCommand;
    std::vector<DT> dt;
}DTHead;

typedef struct TSymbols {
    std::string labelName;
    int address;
}TSymbols;

typedef struct CommandCode {
    std::string name;
    int code;
}CommandCode;

static std::vector<std::string> commands = {"add", "sub", "mult", "div", "jmp", "jmpn", "jmpp", "jmpz", "copy", "load", "store", "input", "output", "stop",
                                            "space", "const", "section", "c_output", "c_input", "s_input", "s_output"};

static std::vector<std::string> invalidASC = {"!", "#", "%", "*", "(", ")", "-", "=", "+", "^", "~", "<", ">", "?", "/", "|", ";", ",", "."};

static std::vector<std::string> textCommands = {"add", "sub", "mult", "div", "jmp", "jmpn", "jmpp", "jmpz", "copy", "load", "store", "input", 
                                                "output", "stop", "c_output", "c_input", "s_input", "s_output"};

static std::vector<std::string> dataCommands = {"space", "const"};

#endif // STRUCTS_H
