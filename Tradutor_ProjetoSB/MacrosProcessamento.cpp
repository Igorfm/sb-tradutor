#include "MacrosProcessamento.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <algorithm>
#include "Utilities.h"
#include <vector>

typedef struct MNT
{
    vector<string> macroName;//NOME DA MACRO
    vector<int> startPosition; //ESSE É O INDICE DE ONDE COMECA NA MDT
    vector<int> numeroParametros; //NUMERO DE PARAMETROS SERVE PARA VERIFICAR SE HA MAIS PARAMETROS QUE O PERMITIDO
    vector<int> linhasDaMacro; //É O INDICE QUE TERMINA A MACRO NA MDT FINAL = START+LINHAS
} MNT;

typedef struct MDT
{
    vector<string> definition; //CORPO DA MACRO
} MDT;

MacrosProcessamento::MacrosProcessamento()
{
    //ctor
}

bool MacrosProcessamento::macroProcessar(char *inputFile, char *outputFile)
{
    MNT macroNameTable;
    MDT macroDefitionTable;

    int startPosition = 0;
    int linhasDaMacro = 0;
    ifstream myReadFile;
    ofstream temp1_macro ("temp1_macroProcessamento.pre"); //arquivo sem linhas e tabulacoes inuteis
    ofstream finalFile (outputFile); //arquivo de reescrita final
    string str;
    cout << endl;
    myReadFile.open(inputFile);
    bool acheiMacro = false; //false signica que nao passou pelo inicio
    vector<string> *outVec = new vector<string>();
    int numeroParametros;

    /** RETIRA AS DEFINICOES DE MACRO DO ARQUIVO FONTE E MONTA MNT E MDT **/
    if (myReadFile.is_open())
    {
        while (!myReadFile.eof())
        {
            getline(myReadFile,str); //Le a linha
            string::size_type position = str.find ("macro");
            string::size_type positionEND = str.find ("endmacro");
            // IDENTIFICA A LINHA QUE EXISTE O INICIO DE UMA MACRO
            if (position != string::npos && acheiMacro == false)
            {
                acheiMacro = true; //indica que encontrou uma macro
                Utilities::SplitString(str," ,",outVec); //divide a string entre os tokens por ' ' e ','
                macroNameTable.macroName.push_back(outVec->at(0).substr(0,outVec->at(0).length()-1)); //macro label name sem :
                //contagem do numero de parametros
                numeroParametros = 0;
                for (unsigned int i=2; i<outVec->size(); i++)
                {
                    numeroParametros++;
                }
                macroNameTable.numeroParametros.push_back(numeroParametros);//numero de parametros da macro encontrada
                macroNameTable.startPosition.push_back(startPosition); //INDICE QUE COMECA A MACRO NO MDT
                //cout << str << endl;
                //LIMPAR O OUTVETOR PARA NAO ACUMULAR VALORES
            }
            //ENCONTROU O FIM DA MACRO - ENDMACRO
            else if (positionEND != string::npos && acheiMacro == true)
            {
                acheiMacro = false;
                //ESTA É A LINHA COM ENDMACRO
                macroDefitionTable.definition.push_back(str); //ADD A LINHA ENDMACRO
                startPosition = macroDefitionTable.definition.size(); //ATUALIZA O START DA NOVA MACRO
                macroNameTable.linhasDaMacro.push_back(linhasDaMacro);
                outVec->clear(); //LIMPA O VETOR AO FIM
                //cout << "achei a linha que TERMINA a macro" << endl;
                //cout << str << endl;
            }
            //PASSOU PELO INICIO DA MACRO MAS NÃO CHEGOU AO FIM
            //DEVE APENAS COPIAR O CONTEUDO ATÉ O ENDMACRO
            else if (acheiMacro == true)
            {
                //cout << str << endl; //LINHA COM PARAMETROS ORIGINAIS
                //COMECA NO 2 POIS [0] = MACRONAME , [1] = MACRO
                for (unsigned int i=2; i<outVec->size(); i++)
                {
                    //verifica se os parametros (um a um )existem na linha
                    string::size_type position = str.find (outVec->at(i));
                    if (position != std::string::npos)
                    {
                        //converte a posicao-1 para string
                        string positionToString = static_cast<ostringstream*>( &(ostringstream() << i-1) )->str();
                        string replaceString = "#" + positionToString; // #position
                        //cout << "Replace:" << replaceString << endl;
                        str.replace(position,outVec->at(i).size(),replaceString);
                    }
                }
                macroDefitionTable.definition.push_back(str);
                linhasDaMacro++;
                //cout << str << endl; //LINHA COM PARAMETROS NA ORDEM QUE APARECEM #1, #2 , ...
            }
            else
            {
                //cout << str << endl;
                temp1_macro << str << endl;
            }
        }
    }
    myReadFile.close();
    temp1_macro.close();

    /** Realiza as expansões das macros **/
    myReadFile.open("temp1_macroProcessamento.pre");
    vector<string> *outVec2 = new vector<string>();
    numeroParametros = 0;
    if (myReadFile.is_open())
    {
        while (!myReadFile.eof())
        {
            getline(myReadFile,str); //Le a linha
            //VERIFICA SE EXISTE MACRO DEFINIDA
            if (macroNameTable.macroName.size() > 0)
            {
                //VERIFICA ALGUM DOS LABELS DA TABELA DE MACROS ESTA PRESENTE NA LINHA LIDA
                for(unsigned int i=0; i<macroNameTable.macroName.size(); i++)
                {
                    string::size_type position = str.find (macroNameTable.macroName.at(i));
                    if (position != std::string::npos)
                    {
                        Utilities::SplitString(str," ,",outVec2); //divide a string entre os tokens por ' ' e ','
                        //cout << "Achei uma macro: " << macroNameTable.macroName.at(i) << endl;
                        //cout << macroNameTable.startPosition.at(i) << endl;
                        //cout << macroNameTable.linhasDaMacro.at(i) << endl;
                        //EXPANDE A MACRO
                        for (int j=0; j<macroNameTable.linhasDaMacro.at(i); j++)
                        {
                            string str2 = macroDefitionTable.definition.at(macroNameTable.startPosition.at(i)+j); //LINHA A SER INSERIDA
                            //cout << str2 << endl;
                            //COMECA DA POSICAO 1 , A 0 É O NOME DA MACRO
                            for (unsigned int k=1; k<outVec2->size(); k++)
                            {
                                //verifica se os parametros (um a um )existem na linha
                                string findString = static_cast<ostringstream*>( &(ostringstream() << k) )->str();
                                string::size_type positionParametro = str2.find(findString); //procura pelo #1, #2, #3
                                if (positionParametro != std::string::npos)
                                {
                                    //cout << outVec2->at(k) << endl;
                                    str2.replace(positionParametro-1,2,outVec2->at(k));
                                    //str2.replace(position,1,outVec2->at(k)); //troca o #i pelo parametro
                                    //cout << str2 << endl;
                                }
                            }
                            //cout << str2 << endl; //LINHA COM OS PARAMETROS SUBISTITUIDOS
                            finalFile << str2 << endl;
                        }
                        //LIMPA O VECTOR PARA NAO ACUMULAR VALORES
                        outVec2->clear();
                    }
                    else
                    {
                        //cout << str << endl; //LINHA SEM MACRO
                        finalFile << str << endl;
                    }
                }
            } else {
                finalFile << str << endl;
            }
        }
    }
    myReadFile.close();
    finalFile.close();

    return true;
}
