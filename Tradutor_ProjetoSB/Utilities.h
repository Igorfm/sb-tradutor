#ifndef UTILITIES_H
#define UTILITIES_H

#include<vector>
#include<string>
#include<cstring>
#include<cstdlib>
#include<sstream>
#include "Enums.h"
#include "Structs.h"

using namespace std;

class Utilities {

public:
    static void SplitString(const string s, const char *delim, vector<string> &out);

    static void SplitString(const string s, const char *delim, vector<string> *out);

    static bool is_number(const string &str, int type);

    static string int2string(int i);

    static bool isLabel(Token t);

    static bool isCommand(Token t);


};
#endif //UTILITIES_H
