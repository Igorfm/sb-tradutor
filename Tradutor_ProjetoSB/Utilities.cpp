#include "Utilities.h"

using namespace std;


    void Utilities::SplitString(const string s, const char *delim, vector<string> &out){

        char *dump = strdup(s.c_str());
        char *token = strtok(dump, delim);

        while( token != NULL ){
            out.push_back(string(token));
            token = strtok(NULL, delim);
        }
        free(dump);
        free(token);
    }

    void Utilities::SplitString(const string s, const char *delim, vector<string> *out){

        char *dump = strdup(s.c_str());
        char *token = strtok(dump, delim);

        while( token != NULL ){
            out->push_back(string(token));
            token = strtok(NULL, delim);
        }
        free(dump);
        free(token);
    }

    bool Utilities::is_number(const string &str, int type)
    {
        if(type == 16){
            return str.find_first_not_of("0123456789ABCDEF") == std::string::npos;
        }else if(type == 10){
            return str.find_first_not_of("0123456789") == std::string::npos;
        }

        return false;
    }

    string Utilities::int2string(int i){
        stringstream ss;
        string str;
        ss << i;
        ss >> str;
        return str;
    }

    bool Utilities::isLabel(Token t){
        return (t.type == Label);
    }

    bool Utilities::isCommand(Token t){
        return (t.type == Command);
    }




