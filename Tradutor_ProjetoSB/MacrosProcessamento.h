#ifndef MACROSPROCESSAMENTO_H
#define MACROSPROCESSAMENTO_H

using namespace std;

class MacrosProcessamento
{
    public:
        MacrosProcessamento();
        bool macroProcessar(char *inputFile, char *outputFile);
    protected:
    private:
};

#endif // MACROSPROCESSAMENTO_H
