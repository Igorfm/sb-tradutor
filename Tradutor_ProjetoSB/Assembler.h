#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include<iostream>
#include<fstream>
#include<cstring>
#include<list>
#include<iostream>
#include<cstdlib>
#include<sstream>
#include<algorithm>
#include<iterator>
#include<map>
#include "Utilities.h"
#include "Structs.h"
#include "algorithm"

using namespace std;

class Assembler{

public:
    vector<Error> error;
    vector<TSymbols> tSymbols;

private:
    ifstream file;
    int lineNumber = 0;
    int currentTokenAddress = 0;
    DTHead dtHead;
    vector<CommandCode> commandCode;
    bool sectionText, sectionData;
    Section currentSection;
    string fileName;
    string outputFile;
    map<string, string> maptoIA32;

public:
    /**
    Constructor
    */
    Assembler(char* fileName, char* outputFile);
    Assembler(string fileName, string outputFile);

    void Start();

private:

    /**
    *\brief Lexicon, semantic ,syntatic analysis and tables
    */
    void Analysis();

    /**
    *\deprecated Utilizado para o primeiro projeto
    */
    void AssemblerCode();

    /**
    *\brief Lexicon analysis, identifying tokens and address
    *@param[in] Token
    *@param[out] All tokens of the line
    */
    void AnalysisToken(const string , vector<Token> &);

    void InitializeDerivativeTree();

    void InitializeIA32Map();

    /**
    *\deprecated Utilizado para o primeiro projeto
    */
    void InitializaCommandCode();

    /**
    *\brief Code semantic analysis
    *@paran[in] Tokens
    */
    void AnalysisSemantics(vector<Token> &tokens);

    /**
    *\brief Assemble Symbols Table
    *@paran[in] Tokens
    */
    void AssembleTS(vector<Token> tokens);

    void TranslatetoIA32();

    void ErrorModel(string message, ErrorType type);

};

#endif // ASSEMBLER_H

