#ifndef ENUMS_H
#define ENUMS_H

enum ErrorType {Lexicon, Syntactic ,Semantic};
enum TokenType {Command, Label, Term, Number, Variable, SectionElement, Undefined};
enum Section {None, Text, Data};

#endif // ENUMS_h

