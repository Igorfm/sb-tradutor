#include "PreProcessamento.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <algorithm>

#define MAX 5

PreProcessamento::PreProcessamento(char *input, char *output)
{
    this->inputFile = inputFile;
    this->outputFile = output;
}

PreProcessamento::PreProcessamento()
{
    this->inputFile = "Nada foi digitado";
    this->outputFile = "Nada foi digitado";
}

//tira as quebras de linha desnecessárias e remove os comentários
bool PreProcessamento::preProcessar(char *inputFile, char *outputFile)
{
    inputFile = strcat(inputFile,".asm");
    //leitura do arquivo de input
    ifstream myReadFile;
    ofstream outfile ("temp1_preProcessamento.pre"); //arquivo sem linhas e tabulacoes inuteis
    ofstream outfile2 ("temp2_preProcessamento.pre"); //arquivo com IFS e EQUS tratados
    ofstream outfile3 (outputFile); //arquivo de reescrita final
    string str;

    myReadFile.open(inputFile);
    if (myReadFile.is_open())
    {
        while (!myReadFile.eof())
        {
            getline(myReadFile,str); //Le string
            if (!str.empty())   //ignora as linhas vazias
            {
                size_t found = str.find_first_not_of(" \t");
                if (found!= string::npos)
                {
                    string str2 = str.substr (found); //sem espaços iniciais e linhas vazias
                    string delimiter = ";";
                    string token = str2.substr(0, str2.find(delimiter));
                    transform(token.begin(), token.end(), token.begin(), ::tolower);
                    for (unsigned int i=0; i<token.length(); i++)
                    {
                        if (token[i] == '\t')   //altera os /t para espaços simples
                        {
                            token[i] = ' ';
                        }
                    }
                    outfile << token << endl;
                    /** println para debug */
                    //cout <<token << endl;
                }
            }
        }
    }
    outfile.close();
    myReadFile.close();

    //Arrays com labels e valores utilizados na diretiva EQU
    string labelArray[MAX];
    string valorArray[MAX];

    cout << endl;

    int tamanhoArray = 0;
    //IDENTIFICAR DIRETAS EQU PRA IDENTICAR LABEL E SEU VALOR - EX: LABEL EQU VALOR
    myReadFile.open("temp1_preProcessamento.pre");
    if (myReadFile.is_open())
    {

        while (!myReadFile.eof())
        {
            getline(myReadFile,str); //Le string
            string::size_type position = str.find ("equ");
            string::size_type positionIF = str.find ("if");
            if (position != string::npos) // IDENTIFICA A POSICAO EM QUE COMEÇA A STRING "EQU"
            {
                string label = str.substr(0,position-1);
                string value = str.substr(position+4,str.length());
                //remove espacos ' ' e '\t' do label
                std::string::iterator end_pos = std::remove(label.begin(), label.end(), ' ');
                std::string::iterator end_pos2 = std::remove(label.begin(), label.end(), '\t');
                label.erase(end_pos, label.end());
                label.erase(end_pos2, label.end());
                //remove os espacos ' ' e '\t' do valor
                end_pos = std::remove(value.begin(), value.  end(), ' ');
                end_pos2 = std::remove(value.begin(), value.end(), '\t');
                value.erase(value.begin(), end_pos-1);
                value.erase(value.begin(), end_pos2-1);

                //atribuição dos valores
                labelArray[tamanhoArray] = label.substr(0,label.length()-1);
                valorArray[tamanhoArray] = value;
                /** println para debug */
                //cout << "label:" << labelArray[i] << endl;
                //cout << "valor:" << valorArray[i] << endl;
                //cout << i << endl;

                //contador de posicao no array
                tamanhoArray++;
            }
            else if (positionIF != string::npos)     //IDENTIFICA A POSICAO EM QUE COMEÇA A STRING "IF"
            {
                //laço para identificar o inicio do label.
                int posicao;
                for (unsigned int i=(positionIF+2); i<str.length(); i++)
                {
                    if (str[i] != ' ')   //quando achar o inicio do label
                    {
                        posicao = i; //indique a posicao inicial
                        break; //saia do laço
                    }
                }
                //copie o label para a string label com base na posicao inicial
                string label = str.substr(posicao,str.length());
                //buscar se o label existe no array de labels
                for (int i=0; i<MAX; i++)
                {
                    if (label.compare(labelArray[i]) == 0)
                    {
                        //cout << label << " encontrado" << endl;
                        getline(myReadFile,str); //leia a prox linha
                        //se valor 1, leia prox linha e escreva no arquivo
                        //se nao ignore a linha lida
                        if (valorArray[i].compare("1") == 0)
                        {
                            // cout << str << endl;
                            outfile2 << str << endl; //escreva no arquivo
                        }
                        break;
                    }
                }
            }
            else
            {
                //se EQU não estiver na linha, copie pro novo arquivo
                outfile2 << str << endl;
                //cout << str << endl;
            }
        }
        outfile2.close();
        myReadFile.close();
    }
    //REESCRITAS DE LABELS PRESENTES NOS ARRAYS
    bool acheiAlgo = false;
    myReadFile.open("temp2_preProcessamento.pre");
    if (myReadFile.is_open())
    {
        //cout << tamanhoArray << endl;
        while (!myReadFile.eof())
        {
            getline(myReadFile,str); //Le string
            //cout << "linha:" << str << endl;
            if ( tamanhoArray > 0 )
            {
                //verifica se existe algum label definido com EQU e subistitui pelo seu valor
                for (int i=0; i<tamanhoArray; i++)
                {
                    //se encontrar a string definida no array de labels, troque a posicao inicial + seu comprimento,
                    //pelo valor correspondente no array de valores
                    std::size_t found = str.find(labelArray[i]);
                    if (found!=std::string::npos)
                    {
                        //cout << labelArray[i] << " found at " << found << endl;
                        str.replace(found,labelArray[i].length(),valorArray[i]);
                        //cout << str << endl;
                        outfile3 << str << endl;
                        acheiAlgo = true;
                    }
                }
                if (acheiAlgo == false )
                {
                    //cout << str << endl;
                    outfile3 << str << endl;
                }
                acheiAlgo = false;
            }
            //SE NAO TIVER DIRETIVAS EQU SOMENTE COPIE O ARQUIVO
            else
            {
                outfile3 << str << endl;
                cout << str << endl;
            }
        }


    }

    outfile3.close();
    myReadFile.close();

    return true;

}
