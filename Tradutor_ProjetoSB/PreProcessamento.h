#ifndef PREPROCESSAMENTO_H
#define PREPROCESSAMENTO_H
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>

using namespace std;

class PreProcessamento
{
    public:
        char *inputFile;
        char *outputFile;
        PreProcessamento();
        PreProcessamento(char *input, char *output);
        bool preProcessar(char *inputFile, char *outputFile);
};

#endif // PREPROCESSAMENTO_H
