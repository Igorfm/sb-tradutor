/***
Igor Fernandes Miranda 11/0013255
Yan Correa Trindade 11/0152794
Compilado no Linux utilizando C++11
**/

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include "PreProcessamento.h"
#include "MacrosProcessamento.h"
#include "Assembler.h"

using namespace std;

int main(int argc, char *argv[])
{
    string outF(argv[3]);
    //printing argc e argv inputs
    cout << "N argumentos: " << argc << endl;
    for (int i=1; i<argc; i++)
    {
        cout << "argv[" << i << "]: " << argv[i] << " ";
    }
    cout << "\n";

    if (argc == 4)
    {
        if (strcmp(argv[1],"-o") == 0) {
            /** Pre Processamento de DIRETIVAS IF E EQU **/
            cout << "\nInicio do Pre Processamento ..." << endl;
            cout << "input: " << argv[2] << ".asm " << "Output: " << argv[3] << ".pre" << endl;
            char *inputFile = argv[2];

            //resolve o problema do output name
            char arquivo[500], arquivoMacro[500];
            strcpy(arquivo,argv[3]);
            strcpy(arquivoMacro,argv[3]);
            strcat(arquivo,".pre");
            strcat(arquivoMacro,".mcr");
            char *outputFile = arquivo;
            //instancia da classe de PreProcessamento
            PreProcessamento preProcess(inputFile,outputFile);
            //preProcessamento e resolve diretivas IF e EQU, alem de formatar o arquivo
            preProcess.preProcessar(inputFile,outputFile);
            //cout << "Fim do processamento de Diretivas EQU e IF\n";
            /** OUTPUT: output.pre **/

            /** Processamento de macros **/
            cout << "Inicio do Processamento de Macros ..." << endl;
            cout << "input: " << outputFile << " " << "Output: " << arquivoMacro << endl;
            //intancia da classe de processamento de Macros
            MacrosProcessamento macroProcess;
            //processamento de macros
            macroProcess.macroProcessar(outputFile,arquivoMacro);
            /** OUTPUT: output.mcr **/

            /** Montador **/
            cout << "Modo -o - Montagem do Programa ..." << endl;
            cout << "input: " << outF << ".mcr " << "Output: " << outF << ".o" << endl;

            Assembler *assembler = new Assembler(outF, outF);
            assembler->Start();

            if(!assembler->error.empty()){
                for(unsigned int i = 0; i < assembler->error.size(); i ++){
                    cout << assembler->error[i].message << endl;
                }
            }else{
                cout << "\nOK" << endl;
            }
        } else {
            cout << "Comando Invalido!" << endl;
        }
    }
    else
    {
        cout << "Numero de argumentos invalido!" << endl;
    }


    /*
    string out("saida");
    string in("asm");
    Assembler *assembler = new Assembler(out, out);
    assembler->Start();*/
    return 0;
}
